all:

.PHONY: theme

install_dirs:
	mkdir -p local var/log var/run var/uploads var/cache var/data/pages

theme_setup:
	@cd theme; yarn

theme:
	@cd theme; yarn build-css
	@cd theme; yarn build-admin-css
