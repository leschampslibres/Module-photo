# Module Photo Low Tech

Le présent module vise à promouvoir la photographie aux Champs Libres, par une entrée directe dans les contenus. Des fiches artistes ainsi que des renvois vers les expositions en cours viennent compléter l'expérience. Ce module web ne constitue pas une exposition en ligne et s'inscrit en rupture par rapport à l'évolution des dispositifs numériques de ce type. Il matérialise également nos engagements en matière de sobriété numérique et particulièrement le soin que nous apportons au développement de solutions qui soient les plus éthiques & les plus respectueuses possible du vivant.

Il est accessible à cette adresse :arrow_forward: https://photo.leschampslibres.fr

Nous nous sommes fixé un certain nombre d'objectifs lors de sa fabrication, dont voici les grandes lignes :

## :high_brightness: 1- SOBRIÉTÉ GRAPHIQUE

- pas de menus
- aucun script d'effets graphiques, seuls 2 types de scripts _JQuery_ sont utilisés : l'un pour transformer certains attributs du langage _Markdown_ en rendu graphique, l'autre pour adapter les photographies entre elles en fonction de l'écran (module de type _Masonry_)
- circulation directe & intuitive au sein des contenus
- design conçu pour le mobile, adaptable à toutes les tailles d'écrans
- thème sur fond noir et respect des contrastes d'accessibilité

## :herb: 2- DÉVELOPPEMENT LOW-TECH

- web statique : ce mini-site est entièrement constitué de fichiers et n'embarque aucune base de données
- fonctionne sous le mini-framework _Flask_, rapide & ultra léger, ne nécessitant quasiment pas de maintenance
- backoffice géré en _Markdown_, langage simplifié permettant de mettre facilement en forme le texte
- _cookie free_ : le module n'embarque aucun traqueur, y compris pour la solution d'analyse d'audience Matomo que nous utilisons à des fins quantitatives & non détaillées
- compatibilité RGPD : ce module ne capte aucune donnée personnelle vous concernant

## :zap: 3- OPTIMISATION DES CONTENUS

- les images sont volontairement limitées à 1000 pixels (pour le plus grand côté)
- recherche du meilleur rendu visuel possible pour un poids de fichier image le plus léger possible. Nous avons ainsi choisi la configuration suivante : export pour le web en JPEG 72dpi /  Progressif non optimisé / Qualité à 60-70% / Converti en sRVB. Ce qui revient à un poids ~150-200Ko par image.
- grande rigueur dans la qualification des métadonnées de chaque fichier image, afin de faciliter l'accessibilité, l'indexation & le référencement naturel
- les cartels des images sont automatiquement traduits en attribut de texte alternatif (_alt_) afin de renforcer l'accessibilité

## :unlock: 4- LICENCES & DROITS D'USAGES

Dans une logique vertueuse d'ouverture & de libération des sources, l'ensemble des éléments du module sont placés sous licence libre (GNU General Public Licence) et partagés ici même, à partir du profil GitLab des Champs Libres. Vous pouvez [le télécharger librement & en intégralité](https://gitlab.com/leschampslibres/Module-photo/-/archive/main/Module-photo-main.zip) ([ZIP](https://gitlab.com/leschampslibres/Module-photo/-/archive/main/Module-photo-main.zip) | [TAR](https://gitlab.com/leschampslibres/Module-photo/-/archive/main/Module-photo-main.tar) | [TAR.GZ](https://gitlab.com/leschampslibres/Module-photo/-/archive/main/Module-photo-main.tar.gz) | [TAR.BZ2](https://gitlab.com/leschampslibres/Module-photo/-/archive/main/Module-photo-main.tar.bz2)), et disposez ainsi des droits suivants :

:white_check_mark: indication de l'auteur : "Les Champs Libres"

:white_check_mark: liberté d'utiliser le module web à n'importe quelle fin

:white_check_mark: liberté de modifier le programme pour répondre à vos besoins

:white_check_mark: liberté de redistribuer des copies à ses amis et voisins

:white_check_mark: liberté de partager avec d'autres les modifications que vous faites

:white_check_mark: les modifications & distributions sont alors soumises à l'emploi de la même licence (copyleft)

:white_check_mark: photographies : "tous droits réservés" (sauf mention contraire)

---

## :bulb: MENTIONS LÉGALES

- Date de création : octobre 2021
- Directrice de la publication : Corinne Poulain
- Equipe projet :
  - conception & pilotage du projet, intégration des contenus : Guillaume Rouan
  - Réalisation graphique : Benjamin Grosfiley ([Approche Design](http://www.approche-design.fr))
  - Réalisation technique : Laurent Delahaie ([Studio web de l'association Bug](https://www.asso-bug.org))
  - Support technique : Matthieu Delsaut

[![Les Champs Libres contribuent à Software Heritage](software-heritage-icon_365x80px.png)](https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.com/leschampslibres/Module-photo)

---

:speech_balloon: N'hésitez pas à nous faire part de vos remarques, elles nous sont très précieuses pour améliorer ce module !

> Les Champs Libres<br />
> 10, cours des Alliés - 35000 RENNES<br />
> [@LesChampsLibres](https://twitter.com/LesChampsLibres)<br />
> communication [arobase] leschampslibres [point] fr

---

![Licence GNU GPL v.3](gnugplv3-icon_136x68.png)

Les Champs Libres | [LesChampsLibres.fr](https://LesChampsLibres.fr) | Rennes

---
