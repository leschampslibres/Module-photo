from os.path import dirname, join

VAR_DIR = join(dirname(dirname(__file__)), 'var')

DATA_DIR = join(VAR_DIR, 'data')

CACHE_TYPE = 'FileSystemCache'
CACHE_DIR = join(VAR_DIR, 'cache')

UPLOAD_DIR = join(VAR_DIR, 'uploads')
