from os import listdir, walk
from os.path import abspath, dirname, join
import click


cmd_folder = abspath(dirname(__file__))


class ComplexCLI(click.MultiCommand):

    def list_commands(self, ctx):
        rv = []
        _, _, files = next(walk(cmd_folder))
        for fname in files:
            if not fname.startswith('_'):
                rv.append(fname[:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            mod = __import__('uniphoto.cli.' + name, None, None, ['cli'])
        except ImportError as e:
            print(e)
            return
        return mod.cli


@click.command(cls=ComplexCLI)
def cli():
    """CLI Univers-Photo."""
    pass

if __name__ == '__main__':
    cli()
