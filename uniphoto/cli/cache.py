import sys
import click
from flask.cli import with_appcontext
from flask import current_app

@click.group()
def cli():
    """
    Gestion du cache
    """
    pass

@cli.command()
@with_appcontext
def clear():
    """
    Vide le cache
    """
    current_app.cache.clear()
