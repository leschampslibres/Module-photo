import sys
import click
from flask.cli import with_appcontext
from ..admin.models import Account, AccountMapper

@click.command()
@click.argument('username')
@with_appcontext
def cli(username):
    """
    Mot de passe d'un compte administrateur
    """
    mapper = AccountMapper()
    account = mapper.get(username)
    if account is None:
        click.echo(f"Création du compte {username}")
        account = Account(username=username,password_hash=None)
    pw = click.prompt("Mot de passe")
    account.set_password(pw)
    mapper.put(account)
