from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask.views import MethodView
from flask_login import LoginManager, login_user, logout_user
from .models import AccountMapper

lm = LoginManager()
lm.login_view = 'auth.login'

mod = Blueprint('auth', __name__)


@lm.user_loader
def load_user(username):
    if username:
        mapper = AccountMapper()
        account = mapper.get(username)
        if account:
            return account
    return lm.anonymous_user


@mod.route('/logout')
def logout():
    logout_user()
    return redirect(url_for(lm.login_view))


#######################################################################################################################

from wtforms import Form
from wtforms.fields import HiddenField, PasswordField, TextField
from wtforms.validators import DataRequired


class LoginForm(Form):
    username = TextField("Login", validators=(DataRequired(),))
    password = PasswordField("Mot de passe", validators=(DataRequired(),))
    next = HiddenField()


#######################################################################################################################


class LoginView(MethodView):

    methods = ('GET', 'POST')

    def get(self):
        next = request.args.get('next', None)
        form = LoginForm(request.form, next=next)
        return render_template('login.html', next=next, form=form, skip_navigation=True)

    def post(self):
        form = LoginForm(request.form)
        mapper = AccountMapper()
        account = mapper.get(form.username.data)
        if account and account.verify_password(form.password.data):
            login_user(account)
            next = form.next.data or url_for('home')
            return redirect(next)
        else:
            flash("Login / mot de passe incorrect.", 'danger')
            return render_template('login.html', next=form.next.data, form=form, skip_navigation=True)


mod.add_url_rule('/login', view_func=LoginView.as_view('login'))
