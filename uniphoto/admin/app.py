from flask import abort, g, redirect, render_template, request, send_from_directory, url_for
from flask_login import current_user
from os.path import dirname, join
from ..core.flaskapp import create_app
from ..models import PageMapper, SiteMetadata
from .auth import mod as auth_module, lm
from .views import mod as views_module

app = create_app(__name__, static_url_path='/admin/static')
lm.setup_app(app)

app.register_blueprint(auth_module, url_prefix='/admin')
app.register_blueprint(views_module, url_prefix='/admin')


@app.before_request
def before_request():
    """
    Réponse 403 pour toutes les pages
    sauf lm.login_view
    """
    g.meta = SiteMetadata()
    if request.endpoint in (lm.login_view, 'static', 'public_static'):
        return
    if current_user.is_anonymous:
        abort(403)


@app.errorhandler(403)
def redirect_login(e):
    """
    Erreur 403 -> redirige vers la page de login
    """
    return redirect(url_for(lm.login_view, next=request.url))


@app.route('/admin/')
def home():
    """
    Accueil admin
    Liste des pages éditables
    """
    pages = PageMapper().keys()
    return render_template('home.html', pages=pages)


@app.route('/static/<path:filename>')
def public_static(filename):
    """
    Assets publics pour le serveur admin debug
    """
    return send_from_directory(join(dirname(app.root_path), 'static'), filename)


if app.debug:

    @app.route('/')
    def debug_root():
        """
        Redirection pour le serveur debug
        """
        from flask import redirect
        return redirect(url_for('home'))
