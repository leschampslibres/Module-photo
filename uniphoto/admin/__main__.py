"""
Admin
Lancement du serveur debug :
    python -m uniphoto.admin
"""
from .app import app
app.run(port=5001, debug=True)
