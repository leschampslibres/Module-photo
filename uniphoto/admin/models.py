from flask import current_app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from dataclasses import dataclass
from os.path import isfile, join
from tempfile import NamedTemporaryFile
from shutil import move


@dataclass
class Account(UserMixin):

    username: str
    password_hash: str
    is_active = True

    def get_id(self):
        return self.username

    def set_password(self, pw):
        self.password_hash = generate_password_hash(pw)

    def verify_password(self, pw):
        if self.password_hash is None:
            return False
        return check_password_hash(self.password_hash, pw)


class AccountMapper():
    """
    Mapper pour un fichier de mots de passe
    Format de stockage :
    une ligne de texte par objet, au format "username:password_hash"
    """

    def __init__(self):
        self.data_fname = join(current_app.config['DATA_DIR'], 'passwd')

    def get(self, username):
        """
        Recherche un compte par l'attribut username
        """
        if not isfile(self.data_fname):
            return None
        match = None
        with open(self.data_fname) as fd:
            for line in fd.readlines():
                if line.startswith(username + ':'):
                    match = line.strip()
                    break
        if match is None:
            return None
        un, pw = match.split(':', 1)
        return Account(username=un, password_hash=pw)

    def put(self, account):
        """
        Enregistre un compte
        """
        new_line = f"{account.username}:{account.password_hash}"
        lines = list()
        exists = False
        if isfile(self.data_fname):
            with open(self.data_fname) as fd:
                for line in fd.readlines():
                    if line.startswith(account.username + ':'):
                        exists = True
                        line = new_line
                    lines.append(line.strip())
        with NamedTemporaryFile('wt', delete=False) as temp:
            for line in lines:
                print(line, file=temp)
            if not exists:
                print(new_line, file=temp)
            temp.close()
            move(temp.name, self.data_fname)
