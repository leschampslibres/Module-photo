from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for
from flask.views import MethodView
from ..models import *

mod = Blueprint('views', __name__)

#######################################################################################################################
from wtforms import Form
from wtforms.fields import TextAreaField, TextField
from wtforms.validators import DataRequired


class MetaForm(Form):
    title = TextField("Titre par défaut")
    description = TextAreaField("Description (SEO)")
    header = TextAreaField("En-tête (html)")
    footer = TextAreaField("Pied de page (html)")
    tracker = TextAreaField("Code de suivi (html)")


class MetaView(MethodView):
    """
    Métadonnées du site
    """
    methods = ('GET', 'POST')

    def get(self):
        form = MetaForm(obj=SiteMetadata())
        return render_template('meta.html', form=form)

    def post(self):
        form = MetaForm(request.form)
        if form.validate():
            meta = SiteMetadata()
            form.populate_obj(meta)
            meta.put()
            current_app.cache.clear()
            flash("Métas enregistrés", 'success')
        return render_template('meta.html', form=form)


mod.add_url_rule('/meta', view_func=MetaView.as_view('meta'))

#######################################################################################################################


class EditForm(Form):
    title = TextField("Titre")
    body = TextAreaField("Corps de texte")


class EditView(MethodView):
    """
    Édition d'une page
    """
    methods = ('GET', 'POST')

    def get(self, slug):
        page = PageMapper().get(slug)
        if page is None:
            abort(404)
        form = EditForm(obj=page)
        return render_template('edit.html', form=form)

    def post(self, slug):
        mapper = PageMapper()
        page = mapper.get(slug)
        if page is None:
            abort(404)
        form = EditForm(request.form)
        if form.validate():
            form.populate_obj(page)
            mapper.put(page)
            current_app.cache.delete(page.slug)
            flash("Page enregistrée", 'success')
            return redirect(url_for('.edit', slug=slug))
        return render_template('edit.html', form=form)


mod.add_url_rule('/edit/<string:slug>', view_func=EditView.as_view('edit'))


@mod.route('/create', methods=('POST',))
def create():
    """
    Création d'une page
    """
    slug = request.form.get('slug', None)
    if not slug:
        flash("Paramètre manquant : slug", 'danger')
        return redirect(url_for('home'))
    mapper = PageMapper()
    page = PageMapper().get(slug)
    if not page:
        page = Page(slug=slug)
        mapper.put(page)
    return redirect(url_for('.edit', slug=slug))


#######################################################################################################################

from werkzeug.utils import secure_filename
from os.path import join


@mod.route('/upload', methods=('POST',))
def upload():
    """
    Téléversement d'image
    Réponse json pour CKEditor
    """
    f = request.files.get('upload')
    fname = secure_filename(f.filename)
    f.save(join(current_app.config['UPLOAD_DIR'], fname))
    return dict(uploaded=True, url=url_for('uploads', filename=fname))
