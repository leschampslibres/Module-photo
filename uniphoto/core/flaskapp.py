from flask import Flask, send_from_directory
from flask_caching import Cache


def create_app(*args, **kwargs):
    """
    Crée l'app flask et charge les extensions
    Ajoute la route 'uploads'
    """
    app = Flask(*args, **kwargs)
    app.config.from_object('uniphoto.default_settings')
    app.config.from_envvar('FLASK_SETTINGS')
    cache = Cache(app)
    setattr(app,'cache',cache)
    def uploads(filename):
        return send_from_directory(app.config['UPLOAD_DIR'], filename)
    app.add_url_rule('/uploads/<path:filename>', view_func=uploads, endpoint='uploads')
    return app
