from flask import abort, render_template
from .core.flaskapp import create_app
from .models import *

app = create_app(__name__)

SKIP_CACHE = False


@app.route('/')
def home():
    """
    Page d'accueil
    """
    return page('index')


@app.route('/<path:slug>')
def page(slug):
    """
    Rendu d'une page avec mise en cache
    """
    meta = SiteMetadata()
    obj = None if SKIP_CACHE else app.cache.get(slug)
    if obj is None:
        mapper = PageMapper()
        obj = mapper.get(slug)
        if obj is None:
            abort(404)
        obj.render()
        if not SKIP_CACHE:
            app.cache.set(slug, obj)
    return render_template('page.html', meta=meta, page=obj)
