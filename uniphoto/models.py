from flask import current_app, safe_join
from os.path import isfile, join
from yaml import CDumper as Dumper, dump, CLoader as Loader, load
from datetime import datetime
from dataclasses import dataclass, asdict
from markdown import markdown
from bs4 import BeautifulSoup

class Page:
    """
    Page du site
    """

    def __init__(self, slug="", title="", body=""):
        self.slug = slug
        self.title = title
        self.body = body
        self.mtime = None
        self.html = None

    def render(self):
        """
        Rendu de body en markdown
        """
        if not self.html:
            html = markdown(self.body)
            soup = BeautifulSoup(html, 'lxml')
            """
            Cherche :
            <body>
                <p>
                    <img ...>
                </p>
                <blockquote>
                    TEXTE
                </blockquote>
            et transforme img et TEXTE en :
                <div class="brick">
                    <figure class="photo">
                        <img alt="TEXTE" ...>
                        <div class="cartel">
                            TEXTE
                        </div>
                    </figure>
                </div>

            si img possède un attribut alt="...", crée un lien :
                <a href="..."><img ></a>
            """
            for para in soup.select('body p'):
                img = para.find('img')
                if img:
                    wrapper = img
                    if img['alt']:
                        a = soup.new_tag('a', attrs={'href': img['alt']})
                        wrapper = img.wrap(a)
                    del img['alt']
                    brick = soup.new_tag('div', attrs={'class': 'brick'})
                    figure = soup.new_tag('figure', attrs={'class': 'photo'})
                    brick.append(figure)
                    para.append(brick)
                    figure.append(wrapper)
                    next_el = para.find_next_sibling()
                    next_sibl = para.find_next_sibling('blockquote')
                    if next_sibl is None:
                        continue
                    if next_sibl == next_el:
                        cartel = soup.new_tag('div', attrs={'class': 'cartel'})
                        alt = [x.string.strip() for x in next_sibl.children]
                        img['alt'] = ' '.join([x for x in filter(lambda x: bool(x), alt)])
                        cartel.extend([x for x in next_sibl.children])
                        figure.append(cartel)
                        next_sibl.decompose()
                    para.unwrap()
            """
            Cherche :
            <body>
                <div class="brick">...</div>
                <div class="brick">...</div>
                ...
                <div class="brick">...</div>
            Et enveloppe les divs :
                <div class="masonry">
                    <div class="brick">...</div>
                    <div class="brick">...</div>
                </div>
                ...
                <div class="masonry">
                    <div class="brick">...</div>
                </div>
            """
            masonry = None
            for brick in soup.select('body div.brick'):
                next_el = brick.find_next_sibling()
                next_sibl = brick.find_next_sibling('div', attrs={'class': 'brick'})
                masonry_close = next_sibl != next_el
                if masonry:
                    masonry.append(brick)
                else:
                    masonry = soup.new_tag('div', attrs={'class': 'masonry'})
                    brick.wrap(masonry)
                if masonry_close:
                    masonry = None

            """
            Cherche :
            <body>
                <table>
                    ...
                    <img alt="URL">
            Et transforme en lien :
                <table>
                    ...
                    <a href="URL"><img ></a>
            """
            for img in soup.select('body table img'):
                if img.has_attr('alt'):
                    a = soup.new_tag('a', attrs={'href': img['alt']})
                    del img['alt']
                    img = img.wrap(a)

            """
            Cherche :
            <p>
                <a>...</a>
            </p>
            Et ajoute les classes "button-wrapper" et "button" :
            <p class="button-wrapper">
                <a class="button">...</a>
            </p>
            """
            for para in soup.select('body p'):
                anchor = para.find('a')
                if anchor and len(para.contents) == 1 and para.contents[0] == anchor:
                    para['class'] = 'button-wrapper'
                    anchor['class'] = 'button'

            self.html = ' '.join([str(x) for x in soup.body.contents])
        return self.html


class PageMapper:
    """
    Mapper pour la classe Page
    Représentation d'une page:
    Fichier <DATA_DIR>/pages/<slug>.md
        ---
        <données yaml en en-tête>
        ...
        <corps de texte au format markdown>
    """

    def __init__(self):
        self.data_dir = join(current_app.config['DATA_DIR'], 'pages')

    def get(self, slug):
        """
        Charge une page
        """
        fname = safe_join(self.data_dir, slug + '.md')
        if isfile(fname):
            with open(fname) as fd:
                text = fd.read()
            page = Page(slug=slug)
            if text.startswith('---'):
                end_frontmatter = text.find('\n...\n')
                frontmatter_text = text[:end_frontmatter]
                if end_frontmatter != -1:
                    page.body = text[end_frontmatter + 5:]
                else:
                    page.body = ''
                frontmatter = load(frontmatter_text, Loader=Loader)
                ts = frontmatter.get('mtime', None)
                page.mtime = datetime.fromisoformat(ts) if ts else None
                page.title = frontmatter.get('title', '')
            else:
                page.body = text
            return page
        else:
            return None

    def put(self, page):
        """
        Sauvegarde une page
        """
        if not page.slug:
            raise ValueError("Missing page.slug")
        slug = page.slug.strip('/')
        fname = safe_join(self.data_dir, slug + '.md')
        data = dict(title=page.title, mtime=datetime.now().isoformat())
        with open(fname, 'w') as fd:
            dump(data, stream=fd, Dumper=Dumper, allow_unicode=True, explicit_start=True, explicit_end=True)
            fd.write(page.body or '')

    def keys(self):
        """
        Liste des pages
        Renvoie les slugs (basenames sans extension des fichers .md)
        """
        from glob import glob
        from os.path import basename
        items = sorted(map(lambda x: basename(x)[:-3], glob(join(self.data_dir, '*.md'))))
        return items


@dataclass(init=False)
class SiteMetadata:
    """
    Métadonnées du site
    """

    title: str = ''
    description: str = ''
    header: str = ''
    footer: str = ''
    tracker: str = ''

    def __init__(self):
        """
        Charge le fichier métas
        """
        self.data_fname = join(current_app.config['DATA_DIR'], 'site-metadata.yml')
        if isfile(self.data_fname):
            with open(self.data_fname) as fd:
                data = load(fd, Loader=Loader)
                try:
                    for k, v in data.items():
                        setattr(self, k, v)
                except AttributeError:
                    pass

    def put(self):
        """
        Sauvegarde les métas
        """
        data = asdict(self)
        with open(self.data_fname, 'w') as fd:
            dump(data, stream=fd, Dumper=Dumper, allow_unicode=True, explicit_start=True)
