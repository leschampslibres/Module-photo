Univers-Photo : Manuel utilisateur
==================================

Première utilisation
--------------------

En console, créer un compte utilisateur et un mot de passe:

exemple, création d'un utilisateiur `admin`

~~~
./cli passwd admin
~~~

Renseigner les différents champs de "Maintenance -> Configuration du site". Les valeurs de ces champs peuvent être
prises depuis une autre installation,  en copiant le fichier `var/data/site-metadata.yml`.

Créer la page d'accueil, elle doit se nommer "index".  Le fichier correspondant est `var/data/pages/index.md`.

Créer une secode page, par exemple "mentions-legales", l'url de cette page  sera alors "/mentions-legales" et son
contenu sera stocké dans le fichier `var/data/pages/mentions-legales.md` au format markdown.

Créer une image liée
--------------------

Cliquer sur l'image et utiliser l'attribut "description" pour entrer l'url du lien.

![](text-alt.png)

Créer un cartel
---------------

Écrire un bloc "citation" juste sous l'image. Le bloc sera alors affiché sur l'image comme un cartel, dépliable avec le
bouton "+". Ne pas dépasser 5 lignes pour éviter que le cartel ne dépasse de l'image.

![](cartel.png)
