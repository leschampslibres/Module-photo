Univers-Photo : Installation
============================

Cette installation se base sur supervisor et gunicorn.
gunicon est un serveur http, il lance les tâches de l'application.
supervisor lance et surveille les processus gunicorn.

L'istallation suppose que le compte utilsateur s'appelle "uniphoto"
et que le dossier de base de l'istallation est `/home/uniphoto`.

L'application se compose de 2 services indépendants, un "site par défaut"
et un service administrateur.

environnement python
--------------------

sous la racine de l'application : `cd /home/uniphoto`

~~~
python3 -m venv --prompt UniversPhoto venv
. environ
pip install -r requirements.txt
pip install -r requirements-prod.txt
make install_dirs
~~~

fichier de configuration : `/home/uniphoto/local/settings.py`

générer une clé secrète,
`python3 -c 'import os; print(os.urandom(24))'`

exemple de fichier `settings.py`

~~~
SECRET_KEY = b'\xd0\xb8+\xbf^\r\xc0T\xbcz:\xd9\x88\x98\xb4<i\xbd+\xb0\xfbH\xbc\x91'
~~~

gunicorn
--------

chaque service possède sa configuration, exemples de configuration:
le service par défaut est connecté au port 5080 on lance 10 tâches serveur.
Le service admin écoute le port 5081, on ne lance qu'unne seule tâche pour
ce service.

fichier `/home/uniphoto/local/gunicorn_config_uniphotodefault.py`

~~~
chdir = '/home/uniphoto'
raw_env = 'FLASK_SETTINGS=/home/uniphoto/local/settings.py'
workers = 10
worker_class = 'gevent'
user = 'uniphoto'
group = 'uniphoto'
errorlog = '-'
loglevel = 'error'
accesslog = '-'
bind = "127.0.0.1:5080"
pidfile = '/home/uniphoto/var/run/uniphotodefault_wsgi.pid'
proc_name = 'gunicorn_uniphotodefault'
~~~

fichier `/home/uniphoto/local/gunicorn_config_uniphotoadmin.py`

~~~
chdir = '/home/uniphoto'
raw_env = 'FLASK_SETTINGS=/home/uniphoto/local/settings.py'
workers = 1
worker_class = 'gevent'
user = 'uniphoto'
group = 'uniphoto'
errorlog = '-'
loglevel = 'error'
accesslog = '-'
bind = "127.0.0.1:5081"
pidfile = '/home/uniphoto/var/run/uniphotoadmin_wsgi.pid'
proc_name = 'gunicorn_uniphotoadmin'
~~~

supervisor
----------

Fichier de configuration `/etc/supervisor/conf.d/univers-photo.conf`

_L'emplacement de ce fichier peut varier selon les distributions linux,
il est donné ici sur debian._

~~~
[program:uniphotodefault_wsgi]
user=uniphoto
directory=/home/uniphoto
command=/home/uniphoto/venv/bin/gunicorn -c "local/gunicorn_config_uniphotodefault.py" uniphoto.app:app
stdout_logfile=/home/uniphoto/var/log/uniphotodefault_wsgi.log
stderr_logfile=/home/uniphoto/var/log/uniphotodefault_werr.log
autostart=True
autorestart=True
numprocs=1
loglevel=info

[program:uniphotoadmin_wsgi]
user=uniphoto
directory=/home/uniphoto
command=/home/uniphoto/venv/bin/gunicorn -c "local/gunicorn_config_uniphotoadmin.py" uniphoto.admin.app:app
stdout_logfile=/home/uniphoto/var/log/uniphotoadmin_wsgi.log
stderr_logfile=/home/uniphoto/var/log/uniphotoadmin_werr.log
autostart=True
autorestart=True
numprocs=1
loglevel=info
~~~

pour démarrer les services :

~~~
sudo supervisorctl
reload
~~~
